# Installation and usage instructions

## Before installing docker

### Clone docker project files
To stark working you need to clone server project https://gitlab.com/sfzx/koa-server
```bash
git clone https://gitlab.com/sfzx/koa-server.git
```

## Docker

### You need to have docker and docker-compose installed on your machine

### Building and running container images
Project has 2 containers at moment:

* Two defaut: Mongodb database, node server

You can build and run the default set by just running the command: `docker-compose up`.

If you want to reset/rebuild the image then run the command:
```bash
docker-compose build <service_name>
```
Where `<service_name>` is main category name from `docker-composer.yml` file.

To reset/rebuild all containers please run:
```bash
docker-compose build [--no-cache]
```
You can add `--no-cache` option to recreate containers by force with re-downlaoding sources.

### Running console and commands inside containers

You can run commands using command:
```bash
docker exec -it <container-name> <command>
```
Where `<container-name>` is the target container.
You can find out disired container name by running `docker ps` and checking out the last column of the list being printed.

The `<command>` here is something you want to run inside dockered container.
For running a console use `/bin/bash`.

For Node and Mongodb containers you can just use `koa-node-shell.sh` and `mongo-shell.sh` files in the manner:
```bash
./node_shell.sh [command]
```
Where `command` is anything you want to run in the container, default command is `/bin/bash` (launching interactive
console).
