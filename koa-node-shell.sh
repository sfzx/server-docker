#!/bin/bash
if [[ -z "$@" ]]
then
    CMD="/bin/bash"
else
    CMD=$@
fi

docker exec -it koa-server $CMD
